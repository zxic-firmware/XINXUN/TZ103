#!/bin/sh

pid=$(ps | grep 'iccid_check' | awk '{print $1}')
if [[ -n "$pid" ]]; then
  kill $pid 2>/dev/null
fi

alias iccid_check=echo
export PATH=/mnt/userdata/etc_rw/sh:$PATH
